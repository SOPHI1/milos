# cimport the Cython declarations for numpy
# cython: language_level=3,
cimport numpy as np
import numpy as np

# ctypedef np.npy_intp SIZE_t
DTYPE_INT = np.intc
DTYPE_DOUBLE = np.float_

# cdefine the signature of our c function
cdef extern from "milos.h":
    void call_milos(
        const int *options,
        const int *size,
        const double *waveaxis,
        const double *weight,
        const double *initial_model,
        const double *inputdata,
        const double *cavity,
        double *outputdata
        )

# create the wrapper code, with numpy type annotations
def _pymilos(
    np.ndarray[int, ndim=1, mode="c"] options not None,
    np.ndarray[double, ndim=1, mode="c"] waveaxis not None,
    np.ndarray[double, ndim=1, mode="c"] weight not None,
    np.ndarray[double, ndim=1, mode="c"] initial_model not None,
    np.ndarray[double, ndim=1, mode="c"] inputdata not None,
    np.ndarray[double, ndim=1, mode="c"] cavity not None,
    np.ndarray[double, ndim=1, mode="c"] outputdata not None
    ):

    size = np.array(inputdata.shape[0])

    call_milos(
        <int*> np.PyArray_DATA(options),
        <int*> np.PyArray_DATA(size),
        <double*> np.PyArray_DATA(waveaxis),
        <double*> np.PyArray_DATA(weight),
        <double*> np.PyArray_DATA(initial_model),
        <double*> np.PyArray_DATA(inputdata),
        <double*> np.PyArray_DATA(cavity),
        <double*> np.PyArray_DATA(outputdata)
        )

    return

def pymilos(
    options,
    input_data,
    waveaxis,
    weight = [1.,10.,10.,4.],
    initial_model = [400.,30.,120.,3.,0.025,1.,0.01,0.15,0.85],
    cavity = np.empty([], dtype=float), 
    ):
    """
    milos Milne-Eddington synthesis ans inversion code
 
    This code solve the radiative transfer equation under the Milne-Eddington approximation. 
    Was primarily written as the initial steps for developing the RTE code onboard the SO/PHI instrument: the RTE inverter.
    The code can synthesize and invert a set of provided Stokes profiles.

    .. note::
        An example of how to use is can be found in the PHI FDT pipeline package: `phi_rte()` using `generate_level2()`.
    

    :param options: inverter options, mandatory

        * Depending on the ``RTE mode``

            * options[0] = wavelength axis dimension ``l``
            * options[1] = maximum number of iterations, defaults to 30
            * options[2] = 0: RTE, 1: CE + RTE, 2: CE
            * options[3] = 0: Inversion, 1-> synthesis 2-> Response functions.
            * options[4] = full width half maximum of spectral PSF (only if PSF is activated) in mA (integer), 0 means NO PSF.
            * options[5] = spectral PSF wavelength steps in mA (integer)
            * options[6] = spectral PSF number of sampling points

    :type options: np.ndarray

    :param input_data: Stokes profiles or input models, depending on the inversion or synthesis mode. 

        * Dimensions are ``[xy,p,l]`` where:

        * ``l`` is the wavelength dimension
        * ``p`` is the polarization dimension
        * ``xy`` is the spatial dimension

    * In synthesis mode dimensions are ``[m x n]`` where:

        * ``m`` is the model dimension
        * ``n`` is the number of models (first 9 elements are the first model)

    * The model is ordered as follows:

		0. magnetic field strength in gauss
		1. magnetic field inclination in degree
		2. magnetic field azimuth in degree
		3. Line absorption :math:`\\eta_0`
		4. Doppler width :math:`\\Delta\\lambda` in angstrom
		5. Damping :math:`a`
		6. LoS velocity in km/s
		7. Source function :math:`S_0`
		8. Source function :math:`S_1`


    :type input_data: np.ndarray
    :param waveaxis: wavelength axis, in Angstrom. Should be same length as ``data``, e.g., ``l``
    :type waveaxis: np.ndarray
    :param weight: stokes profiles weights, defaults to ``[1,10,10,4]``; values currently used for HRT: ``[1, 4, 5.4, 4.1]``
    :type weight: np.ndarray, optional

    :param initial_model:

        Milne-Eddington initial model, defaults to ``[400,30,120,3,0.0205,1.0,0.01,0.15,0.85]``

        * The initial model corresponds to (in order):

            0. Magnetic field strength. Defaults to 400 G.
            1. Magnetic field inclination. Defaults to 30 degree.
            2. Magnetic field azimuth. Defaults to 120 degree.
            3. Line absorption :math:`\\eta_0`. Defaults to 3.
            4. Doppler width :math:`\\Delta\\lambda`. Defaults to 0.0205 Angstrom.
            5. Damping :math:`a`. Defaults to 1.0.
            6. LoS velocity. Defaults to 0.01 km/s.
            7. Source function :math:`S_0`. Defaults to 0.15.
            8. Source function :math:`S_1`. Defaults to 0.85.

    :type initial_model: list, optional
    :param cavity: _description_, defaults to np.empty([], dtype=float)
    :type cavity: np.ndarray, optional
    :param cavity: cavity map in Angstrom (xy dimensions), defaults to np.empty([], dtype=float)
    :type cavity: np.ndarray, optional
    :raises ValueError: _description_
    :raises ValueError: _description_
    :return: results

        * in the inversion mode the results are stored in a ``[xy,k]`` where ``k`` corresponds to the following 12 output model parameters

			0. pixel counter
			1. iterations number
			2. magnetic field strength in gauss
			3. magnetic field inclination in degree
			4. magnetic field azimuth in degree
			5. Line absorption :math:`\\eta_0`
			6. Doppler width :math:`\\Delta\\lambda` in angstrom
			7. Damping :math:`a`
			8. LoS velocity in km/s
			9. Source function :math:`S_0`
			10. Source function :math:`S_1`
			11. merit function final value.

        * In the synthesis mode the results are stored in a ``[n,s,w]`` where:

            * ``n`` is the number of models
            * ``s`` is the polarization dimension [wave, Stokes I, Q, U, V]
            * ``w`` is the number of wavelength points

    :rtype: np.ndarray

    .. warning::
         Continuum point can be either side and wavelength axis may be in any order. However, the program looks automatically for its position
         looking at the wavelength point that has the largest wave step. This behaviour will be change in the future.

    The milos code has an execulable (ASCII version) ``src/lib/milos.x``. The user shall use the proper PATH to use it. 
    Documentation will be added soon.
    """

    def __check_par(par, oftype = np.float_):
        if not type(par) is np.ndarray:
            par = np.array(par)
        if par.dtype != oftype:
            par = par.astype(oftype)
        if par.flags['C_CONTIGUOUS'] != True:
            print('non contiguous vector')
            par = par.copy(order='C')
        return par

    options = __check_par(options,oftype=DTYPE_INT)
    waveaxis = __check_par(waveaxis,oftype=DTYPE_DOUBLE)
    weight = __check_par(weight,oftype=DTYPE_DOUBLE)
    initial_model = __check_par(initial_model,oftype=DTYPE_DOUBLE)
    input_data = __check_par(input_data,oftype=DTYPE_DOUBLE)

    if options.shape[0] != 9:
        print("pymilos options not valid: Error en el numero de parametros: %d . Pruebe: milos NLAMBDA MAX_ITER CLASSICAL_ESTIMATES RFS [FWHM(in A) DELTA(in A) NPOINTS] perfil.txt\n")
        print("options = (NLAMBDA, MAX_ITER, CLASSICAL_ESTIMATES, RFS, FWHM(in A), DELTA(in A), NPOINTS, S0S1, ILAMBDA)")
        print("Note : CLASSICAL_ESTIMATES=> 0: Disabled, 1: Enabled, 2: Only Classical Estimates.")
        print("RFS : 0: Disabled     1: Synthesis      2: Synthesis and Response Functions")
        raise ValueError("Error in options")
    else:
        print("Options [in pymilos]: ", options)
        assert options[0] == len(waveaxis)

    # check if we are in synthesis or inversion mode:
    if options[3] in {1,2}: #Synthesis mode

        nmodels = len(input_data)//9
        print('models (milos.pyx): ',nmodels)

        input_data = input_data.flatten(order='C')
        print('------ flattened: ',input_data.shape)
            
        if not cavity.shape:
            cavity=np.zeros((nmodels),dtype=DTYPE_DOUBLE)
        else:
            assert cavity.size == nmodels
        cavity = __check_par(cavity,oftype=DTYPE_DOUBLE)

        if options[3] == 1: #No RFS
            output_data = np.zeros((len(waveaxis)*4*nmodels),dtype=DTYPE_DOUBLE)
        elif options[3] == 2: #RFS  
            output_data = np.zeros((10*len(waveaxis)*4*nmodels),dtype=DTYPE_DOUBLE)

        _pymilos(options,waveaxis,weight,initial_model,input_data,cavity,output_data)

        if options[3] == 1: #No RFS
            if nmodels == 1:
                return np.reshape(output_data,(4,len(waveaxis)))
            else:
                return np.reshape(output_data,(nmodels,4,len(waveaxis)))
        if options[3] == 2: #RFS 
            if nmodels == 1:
                return np.reshape(output_data,(10,4,len(waveaxis))) 
            else:
                return np.reshape(output_data,(nmodels,10,4,len(waveaxis))) 

    if options[3] == 0: #Inversion mode

        #prepare input
        nyx, npol, nwave = input_data.shape
        print('input_data shape [in pymilos]: ',nyx, npol, nwave)

        input_data = input_data.flatten(order='C')
        print('------ flattened: ',input_data.shape)

        print("Weights  [in pymilos]: ",weight)
        print("Initial Model  [in pymilos]: ",initial_model)

        if not cavity.shape:
            cavity=np.zeros((nyx),dtype=DTYPE_DOUBLE)
        else:
            assert cavity.size == nyx
        cavity = __check_par(cavity,oftype=DTYPE_DOUBLE)

        length = len(input_data)
        output_data = np.zeros((length//len(waveaxis)//4 * 12),dtype=DTYPE_DOUBLE)

        _pymilos(options,waveaxis,weight,initial_model,input_data,cavity,output_data)

        return np.reshape(output_data,(nyx,12))

    else:
        print('ERROR')
        return 0
