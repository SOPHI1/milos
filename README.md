<div style="width:800px">

<img src="docs/images/SPGLOGO-LR.png" align="right" width="60px" />
<img src="docs/images/S3PC_HR.png" align="right" width="60px" />
<img src="docs/images/SOPHI-Logo.png" align="right" width="54px" />
<br>

# MILOS


This repository contains the Milne-Eddington inversion code developed by the Solar Physics Group at the [Instituto de Astrofísica de Andalucía](https://www.iaa.es) within the Spanish Space Solar Physics Consortium (S3PC). This code was used for prototyping the Radiative Transfer Equation inversion FPGA code of the Polarimetric and Helioseismic Imager onboard the Solar Orbiter ESA mission.

For more information about the Solar Physics Group, visit [spg.iaa.es](http://spg.iaa.es/), and learn about the S3PC at [s3pc.es](http://s3pc.es/).

Additional inversion tools can be found on the main website and in the following repositories:
- [https://github.com/IAA-InvCodes/P-MILOS](https://github.com/IAA-InvCodes/P-MILOS)
- [https://github.com/vivivum/MilosIDL](https://github.com/vivivum/MilosIDL)

</div>

If you use this code, please cite it accordingly: 
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10522332.svg)](https://doi.org/10.5281/zenodo.10522332)

## Installation

Milos is based on C and Cython 3+ and can be easily installed in your environment.

You can install it via pip (not active yet) or directly download it from here:

```shell
pip install milos@git+https://gitlab.com/SOPHI1/milos
```

If you download the source, you can choose to install it in a specific environment:

```shell
pip install -e .   OR   pip install -r requirements.txt -e .

```

As you wish.

If you want to update to last version:

```shell
pip install milos --upgrade
```

Getting started
------------

Package is loaded in the stardard way:

```python
import milos as pym
out = pym.pymilos(...)
```
OR
```python
import milos 
out = milos.pymilos(...)
```

Using it may require some patience as the documentation is still a work in progress. We apologize for any inconvenience.

There are additional tools that are useful:
```python
import milos as pym
pym.phi_rte(...) # wrapping routine for PHI
@pym.timeit
```
etc

Authors and acknowledgment
------------

David Orozco Suárez (orozco@iaa.es)

This project is funded by AEI/MCIN/10.13039/501100011033/ (RTI2018-096886-C5, PID2021-125325OB-C5, PCI2022-135009-2, PCI2022-135029-2) and ERDF “A way of making Europe” and “Center of Excellence Severo Ochoa” awards to IAA-CSIC (SEV-2017-0709, CEX2021-001131-S)


----
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

