from setuptools import Extension,setup
from setuptools.command.install import install
from Cython.Build import cythonize
import subprocess
import numpy

def compile_and_install_software():
    subprocess.check_call('make -C milos/lib/ libmilos.a', shell=True)
    subprocess.check_call('make -C milos/lib/ milos.x', shell=True)
class CustomInstall(install):
    compile_and_install_software()

compile_extra_args = []
link_extra_args = []

# if platform.system() == "Windows":
#     compile_extra_args = ["/std:c++latest", "/EHsc"]
# elif platform.system() == "Darwin":
#     compile_extra_args = ["-mmacosx-version-min=13.00"]
#     link_extra_args = ["-mmacosx-version-min=13.00"]

ext_modules = Extension(
    name = "pymilos",  #pymilos
    packages=['pymilos'],  #pymilos
    sources=["milos/lib/pymilos.pyx"],
    libraries=["milos"],
    library_dirs=["milos/lib"],
    include_dirs=[numpy.get_include()],
    extra_compile_args = compile_extra_args,
    extra_link_args = link_extra_args
    # define_macros=[("NPY_NO_DEPRECATED_API", "NPY_1_7_API_VERSION")]
    )

setup(
    cmdclass={'install': CustomInstall},
    ext_modules=cythonize([ext_modules])
)